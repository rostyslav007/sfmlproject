#pragma once

#include "AlgoStructs.h"

#include "States.h"
#include <algorithm>
#include <cassert>
#include <memory>
#include <vector>
#include <unordered_set>
#include <unordered_map>

namespace geometry {
    const auto eps = 1e-9;

    class DelaunayBuilder {

    public:
        DelaunayBuilder(const DelaunayBuilder&) = delete;
        const DelaunayBuilder& operator=(const DelaunayBuilder&) = delete;
        DelaunayBuilder(DelaunayBuilder&&) = default;
        DelaunayBuilder& operator=(DelaunayBuilder&&) = default;

        const DelaunayTriangulation& Get() const { return triangulation_; }
        State GetState() const { return state; }
        
        List GetConvexHull() { return convex_hull_; }

        void saveState(Triangulation graph, std::vector<Vector2D> points);

        static std::unique_ptr<DelaunayBuilder> Create(std::vector<Vector2D> points, State& state);

    private:
        State state;
        explicit DelaunayBuilder(std::vector<Vector2D> points) {
            triangulation_.points = std::move(points);
        }

        // ���� ������������
        void Build(State& state);

        void AddPointToTriangulation(int i);

        // �������, �� ���������� ���������� ������������
        void FixTriangulation(int left, int right, int outer);

        // ��������, �� ������� �������������� �������������
        bool CheckDelaunayCondition(int left, int right, int outer, int inner) const;

        // ������� ������� �� ��������� ����� ��� �� ���� ������������
        std::vector<Edge> recursion_stack_{};

        // ��������� ��� ��������� ������������: ����� -> ���� ������ + �����
        DelaunayTriangulation triangulation_{};

        List convex_hull_{};
    };

    // ������� ������� �������� �� ������ ������� ������ points � ���
    std::unordered_set<int> BuildConvexHull(const Triangulation& triangulation);

}