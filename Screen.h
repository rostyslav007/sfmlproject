#include <SFML/Graphics.hpp>

#include "DelaunayAlgo.h"
#include "AlgoStructs.h"

#include "Button.h"
#include "Point.h"
#include "Line.h"
#include "States.h"
#include <string>
#include <vector>
#include <fstream>
#include <unordered_set>
#include <iostream>

class Screen {
public:

	Screen(int width, int height, std::string background_path) {
		window = new sf::RenderWindow(sf::VideoMode(width, height), "Hello!");
		background.loadFromFile(background_path);
		font.loadFromFile("Lobster-Regular.ttf");
	}

	void Run() {

		Button btn1("Clear", {150, 50}, 40, sf::Color::Blue, sf::Color::White, font);
		btn1.setPosition({window->getSize().x - btn1.getWidth() - 50, window->getSize().y / 2 - btn1.getHeight() / 2});

		Button btn2("Convex", { 150, 50 }, 40, sf::Color::Blue, sf::Color::White, font);
		btn2.setPosition({ window->getSize().x - btn2.getWidth() - 50, window->getSize().y / 2 - btn2.getHeight() / 2 - 100 });

		Button btn3("Delaunay", { 150, 50 }, 30, sf::Color::Blue, sf::Color::White, font);
		btn3.setPosition({ window->getSize().x - btn3.getWidth() - 50, window->getSize().y / 2 - btn3.getHeight() / 2 + 100});

		Button btn4("Random", { 150, 50 }, 30, sf::Color::Blue, sf::Color::White, font);
		btn4.setPosition({ window->getSize().x - btn4.getWidth() - 50, window->getSize().y / 2 - btn4.getHeight() / 2 + 200 });

		Button btn5("File", { 150, 50 }, 30, sf::Color::Blue, sf::Color::White, font);
		btn5.setPosition({ window->getSize().x - btn5.getWidth() - 50, window->getSize().y / 2 - btn5.getHeight() / 2 - 200 });

		Button btn6("Voronoi", { 150, 50 }, 40, sf::Color::Blue, sf::Color::White, font);
		btn6.setPosition({ window->getSize().x - btn6.getWidth() - 50, window->getSize().y / 2 - btn6.getHeight() / 2 - 300 });

		Line line(Point(5, 3, 10), Point(5, 5, 10));

		State st(window);
		states = st;

		bool convexShow = false;

		while (window->isOpen())
		{
			sf::Event event;
			while (window->pollEvent(event))
			{
				switch (event.type)
				{
					case sf::Event::Closed:
						window->close();

						break;
					case sf::Event::MouseMoved:
						if (btn1.isMouthOver(*window)) {
							btn1.setBgColor(sf::Color(25, 120, 45));
							btn1.setTxtColor(sf::Color::White);
						}
						else {
							btn1.setBgColor(sf::Color::Blue);
							btn1.setTxtColor(sf::Color::Black);
						}

						break;
					case sf::Event::MouseButtonPressed:
						if (btn1.isMouthOver(*window)) {
							points.clear();
							stack.clear();
							states.clear();
							convexShow = false;
							
						}
						else if (btn2.isMouthOver(*window)) {
							if (points.size() > 2 && !states.animationRun()) {
								buildConvexShell();
								convexShow = true;
							}
						}
						else if (btn3.isMouthOver(*window)) {
							DelaunayTriangulation();
						}
						else if (btn4.isMouthOver(*window)) {
							initializeRandomPoints(10);
						}
						else if (btn5.isMouthOver(*window)) {
							initializePointsFromFile("points.txt");
						}
						else if (btn6.isMouthOver(*window)) {
							VoronoiDiagram();
						}
						else {
							Point pnt(sf::Mouse::getPosition().x - window->getPosition().x - 10, 
								sf::Mouse::getPosition().y - window->getPosition().y - 40, 30);
							if (!pointRepeat(pnt)) {
								points.push_back(pnt);
							}
						}

						break;
					default:
						break;
				}

			}

			window->clear(sf::Color::Blue);
			drawScreen();

			if (!states.animationRun() && convexShow) {
				drawConvexShell();
			}

			
			if (states.stateCount()) {
				states.showState();
			}

			drawPoints();

			btn1.draw(*window);
			btn2.draw(*window);
			btn3.draw(*window);
			btn4.draw(*window);
			btn5.draw(*window);
			btn6.draw(*window);
			window->display();
		}
	}

	bool pointRepeat(Point p) {
		bool rep = false;
		for (Point pnt : points) {
			if (pnt.x == p.x && pnt.y == p.y) {
				rep = true;
				break;
			}
		}
		return rep;
	}

	void initializeRandomPoints(int count) {
		srand(time(nullptr));
		for (int i = 0;i < count;i++) {
			auto x = rand() % 1000 + 100;
			auto y = rand() % 600 + 100;
			points.push_back(Point(x, y, 30));
		}
	}

	void initializePointsFromFile(std::string path) {
		std::string row;
		std::ifstream file(path);
		while (getline(file, row)) {
			int space_pos = row.find(" ");
			int x = std::stoi(row.substr(0, space_pos));
			int y = std::stoi(row.substr(space_pos + 1));
			points.push_back(Point(x, y, 30));
		}
	}

	struct {
		bool operator()(Line l1, Line other) const {
			return l1.k > other.k || (l1.k == other.k && l1.p2.x < other.p2.x);
		}
	} customLess;

	void buildConvexShell() {
		Point minPoi = minPoint();
		states.add({ minPoi });
		std::vector<Line> lines;
		for (Point& p : points) {
			if (minPoi.x != p.x && minPoi.y != p.y) {
				lines.push_back(Line(minPoi, p));
			}
		}
		std::sort(lines.begin(), lines.end(), customLess);

		std::vector<Point> sorted_points = { minPoi };
		for (Line l : lines) {
			sorted_points.push_back(l.p2);
		}

		stack = {minPoi, sorted_points[1]};
		addImage();
		sorted_points.push_back(minPoi);
		int i = 2;
		while (i < sorted_points.size()) {
			while (i < sorted_points.size() && Line::_vectorProd( Line(stack[stack.size()-1], stack[stack.size() - 2]), 
									  Line(sorted_points[i], stack[stack.size() - 1])) <= 0) {
				stack.push_back(sorted_points[i]);
				addImage();
				i++;
			}
			while (i < sorted_points.size() && Line::_vectorProd(Line(stack[stack.size() - 1], stack[stack.size() - 2]),
									 Line(sorted_points[i], stack[stack.size() - 1])) > 0) {
				stack.pop_back();
				addImage();
			}
		}
		stack.push_back(sorted_points[sorted_points.size() - 1]);
		addImage();

		stack.push_back(minPoi);
		addImage();
	}

	void addImage() {
		states.add(stack);
	}

	std::vector<geometry::Vector2D> getPointsFromUser() {
		std::vector<geometry::Vector2D> delaunayPoints;
		for (auto& p : points) {
			delaunayPoints.push_back(geometry::Vector2D(p.x, p.y));
		}

		std::sort(delaunayPoints.begin(), delaunayPoints.end(), [](auto& lhs, auto& rhs) {
			return lhs.x < rhs.x;
		});

		return delaunayPoints;
	}

	geometry::DelaunayTriangulation DelaunayTriangulation() {

		auto points = getPointsFromUser();
		auto builder = geometry::DelaunayBuilder::Create(std::move(points), states);
		auto& triangulation = builder->Get();
		convexHull = builder->GetConvexHull();
		auto convex_hull = geometry::BuildConvexHull(triangulation.graph);
		states = builder->GetState();

		return triangulation;
	}


	bool inConvex(int v1, int v2) {
		for (auto p : convexHull) {
			if (p.left == v1 || p.right == v2) {
				return true;
			}
		}
		return false;
	}

	bool obtuceTr(geometry::Vector2D p1, geometry::Vector2D p2, geometry::Vector2D p3) {
		float l1 = (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y),
			  l2 = (p2.x - p3.x) * (p2.x - p3.x) + (p2.y - p3.y) * (p2.y - p3.y),
			  l3 = (p1.x - p3.x) * (p1.x - p3.x) + (p1.y - p3.y) * (p1.y - p3.y);
		std::vector<float> len = { l1, l2, l3 };
		std::sort(len.begin(), len.end());
		return len[2] > len[1] + len[0];
	}

	void VoronoiDiagram() {
		std::vector<Point> draw_pnts;
		auto triangulation = DelaunayTriangulation();
		auto graph = triangulation.graph;
		auto pnts = triangulation.points;
		for (auto p : convexHull) {
			std::cout << p.left << " " << p.right << std::endl;
		}
		std::cout << "//////////////////////////////////////\n";
		for (auto triangle : graph) {
			int left = triangle.first.v1;
			int right = triangle.first.v2;
			int outer = triangle.second.v1;

			std::cout << left << " " << right << " ";

			geometry::Vector2D left_p = pnts[left];
			geometry::Vector2D right_p = pnts[right];
			geometry::Vector2D outer_p = pnts[outer];

			auto first_pair = getTriangleCenter(left_p, right_p, outer_p);

			int neigh_p1 = -1, neigh_p2 = -1, neigh_p3 = -1;

			float x_n1, y_n1, x_n2, y_n2, x_n3, y_n3;

			if (inConvex(left, right)) {
				if (graph.find(geometry::Edge{ left, right }) != graph.end()) {
					std::cout << "ok ";
					if (!obtuceTr(pnts[left], pnts[right], pnts[graph[geometry::Edge{ left, right }].v1])) {
						auto p1 = pnts[left], p2 = pnts[right];
						auto n_p = getTriangleCenter(left_p, right_p, pnts[graph[geometry::Edge{ left, right }].v1]);
						float x_c = (p1.x + p2.x) / 2, y_c = (p1.y + p2.y) / 2;
						draw_pnts.push_back(Point(n_p.first, n_p.second));
						draw_pnts.push_back(Point(x_c, y_c));
					}
				}
			}

			if (inConvex(right, left)) {
				if (graph.find(geometry::Edge{ right, left }) != graph.end()) {
					if (!obtuceTr(pnts[right], pnts[left], pnts[graph[geometry::Edge{ right, left }].v1])) {
						std::cout << "ok ";
						auto p1 = pnts[right], p2 = pnts[left];
						auto n_p = getTriangleCenter(right_p, left_p, pnts[graph[geometry::Edge{ right, left }].v1]);
						float x_c = (p1.x + p2.x) / 2, y_c = (p1.y + p2.y) / 2;
						draw_pnts.push_back(Point(n_p.first, n_p.second));
						draw_pnts.push_back(Point(x_c, y_c));
					}
				}
			}

			if (inConvex(left, outer)) {
				if (graph.find(geometry::Edge{ left, outer }) != graph.end()) {
					if (!obtuceTr(pnts[left], pnts[outer], pnts[graph[geometry::Edge{ left, outer }].v1])) {
						std::cout << "ok ";
						auto p1 = pnts[left], p2 = pnts[outer];
						auto n_p = getTriangleCenter(left_p, outer_p, pnts[graph[geometry::Edge{ left, outer }].v1]);
						float x_c = (p1.x + p2.x) / 2, y_c = (p1.y + p2.y) / 2;
						draw_pnts.push_back(Point(n_p.first, n_p.second));
						draw_pnts.push_back(Point(x_c, y_c));
					}
				}
			}

			if (inConvex(outer, left)) {
				if (graph.find(geometry::Edge{ outer, left }) != graph.end()) {
					if (!obtuceTr(pnts[outer], pnts[left], pnts[graph[geometry::Edge{ outer, left }].v1])) {
						std::cout << "ok ";
						auto p1 = pnts[outer], p2 = pnts[left];
						auto n_p = getTriangleCenter(outer_p, left_p, pnts[graph[geometry::Edge{ outer, left }].v1]);
						float x_c = (p1.x + p2.x) / 2, y_c = (p1.y + p2.y) / 2;
						draw_pnts.push_back(Point(n_p.first, n_p.second));
						draw_pnts.push_back(Point(x_c, y_c));
					}
				}
			}

			if (inConvex(right, outer)) {
				if (graph.find(geometry::Edge{ right, outer }) != graph.end()) {
					if (!obtuceTr(pnts[right], pnts[outer], pnts[graph[geometry::Edge{ right, outer }].v1])) {
						auto p1 = pnts[right], p2 = pnts[outer];
						auto n_p = getTriangleCenter(right_p, outer_p, pnts[graph[geometry::Edge{ right, outer }].v1]);
						float x_c = (p1.x + p2.x) / 2, y_c = (p1.y + p2.y) / 2;
						draw_pnts.push_back(Point(n_p.first, n_p.second));
						draw_pnts.push_back(Point(x_c, y_c));
					}
				}
			}

			if (inConvex(outer, right)) {
				if (graph.find(geometry::Edge{ outer, right }) != graph.end()) {
					if (!obtuceTr(pnts[outer], pnts[right], pnts[graph[geometry::Edge{ outer, right }].v1])) {
						std::cout << "ok ";
						auto p1 = pnts[outer], p2 = pnts[right];
						auto n_p = getTriangleCenter(outer_p, right_p, pnts[graph[geometry::Edge{ outer, right }].v1]);
						float x_c = (p1.x + p2.x) / 2, y_c = (p1.y + p2.y) / 2;
						draw_pnts.push_back(Point(n_p.first, n_p.second));
						draw_pnts.push_back(Point(x_c, y_c));
					}
				}
			}

			if (graph.find(geometry::Edge{ left, right }) != graph.end()) {
				neigh_p1 = graph[geometry::Edge{ left, right }].v1;
				
				auto n_p = getTriangleCenter(left_p, right_p, pnts[neigh_p1]);

				draw_pnts.push_back(Point(first_pair.first, first_pair.second));
				draw_pnts.push_back(Point(n_p.first, n_p.second));

			} else if (graph.find(geometry::Edge{ right, left }) != graph.end()) {
				neigh_p1 = graph[geometry::Edge{ right, left }].v1;
				
				auto n_p = getTriangleCenter(right_p, left_p, pnts[neigh_p1]);

				draw_pnts.push_back(Point(first_pair.first, first_pair.second));
				draw_pnts.push_back(Point(n_p.first, n_p.second));
			}

			
			if (graph.find(geometry::Edge{ left, outer }) != graph.end()) {
				neigh_p2 = graph[geometry::Edge{ left, outer }].v1;
				
				auto n_p = getTriangleCenter(left_p, outer_p, pnts[neigh_p2]);

				draw_pnts.push_back(Point(first_pair.first, first_pair.second));
				draw_pnts.push_back(Point(n_p.first, n_p.second));
			}else if (graph.find(geometry::Edge{ outer, left }) != graph.end()) {
				neigh_p2 = graph[geometry::Edge{ outer, left }].v1;
				
				auto n_p = getTriangleCenter(outer_p, left_p, pnts[neigh_p2]);

				draw_pnts.push_back(Point(first_pair.first, first_pair.second));
				draw_pnts.push_back(Point(n_p.first, n_p.second));
			}

			

			if (graph.find(geometry::Edge{ right, outer }) != graph.end()) {
				neigh_p3 = graph[geometry::Edge{ right, outer }].v1;

				auto n_p = getTriangleCenter(right_p, outer_p, pnts[neigh_p3]);

				draw_pnts.push_back(Point(first_pair.first, first_pair.second));
				draw_pnts.push_back(Point(n_p.first, n_p.second));

			} else if (graph.find(geometry::Edge{ outer, right }) != graph.end()) {
				neigh_p3 = graph[geometry::Edge{ outer, right }].v1;

				auto n_p = getTriangleCenter(outer_p, right_p, pnts[neigh_p3]);

				draw_pnts.push_back(Point(first_pair.first, first_pair.second));
				draw_pnts.push_back(Point(n_p.first, n_p.second));
			}


			states.addLinePair(draw_pnts);
		}
	}

	std::pair<float, float> getTriangleCenter(geometry::Vector2D p1, geometry::Vector2D p2, geometry::Vector2D p3) {
		Line side1 = Line(Point(p1.x, p1.y), Point(p2.x, p2.y));
		Line side2 = Line(Point(p2.x, p2.y), Point(p3.x, p3.y));
		float x1=(p1.x + p2.x)/2, y1 = (p1.y + p2.y)/2, x2=(p2.x + p3.x)/2, y2=(p2.y + p3.y)/2;
		float k11 = -1 / side1.k;
		float b1 = y1 - k11 * x1;

		float k22 = -1 / side2.k;
		float b2 = y2 - k22 * x2;

		float y_intersect = (b2 * k11 - b1 * k22) / (k11 - k22);
		float x_intersect = (y_intersect - b1) / k11;
		return std::make_pair(x_intersect, y_intersect);
	}

	Point minPoint() {
		Point minPoint = points[0];
		float x = minPoint.x, y = minPoint.y;
		for (Point& p : points) {
			if (p.x < x) {
				x = p.x;
				y = p.y;
				minPoint = p;
			}
		}

		return minPoint;
	}

	void drawPoints() {
		for (Point& p : points) {
			p.draw(*window);
		}
	}

	void drawConvexShell() {
		for (int i = 1;i < stack.size();i++) {
			Line l(stack[i], stack[i - 1]);
			l.draw(*window);
		}
	}

	void drawScreen() {
		setBackgroundScale();
		window->draw(sprite);
	}

	void setBackgroundScale() {

		sprite.setScale(sf::Vector2f(1, 1));
		sprite = sf::Sprite(background);
		sf::Vector2u size = window->getSize();
		double w = size.x;
		double h = size.y;
		double sc_w = background.getSize().x;
		double sc_h = background.getSize().y;
		sprite.setScale(sf::Vector2f(( w / sc_w ), (h / sc_h)));
	}

private:
	State states;
	geometry::List convexHull;
	std::vector<Point> stack;
	std::vector<Point> points;
	sf::Font font;
	sf::Sprite sprite;
	sf::Texture background;
	sf::RenderWindow *window;
};
