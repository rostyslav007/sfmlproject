#pragma once

#include "AlgoStructs.h"

#include <vector>
#include <SFML/Graphics.hpp>
#include "Line.h"
#include "Point.h"
#include <chrono>

class State {
public:

	State(){}

	State(sf::RenderWindow* window) {
		this->window = window;
	}

	void drawPoints() {
		if (points.size() == 0) {
			return;
		}
		for (Point& p : points[0]) {
			p.draw(*window);
		}
	}

	void addLinePair(std::vector<Point> pnts) {
		points.push_back(pnts);
		std::vector<Line> line;
		for (int i = 1;i < pnts.size();i += 2) {
			line.push_back(Line(pnts[i], pnts[i-1]));
		}
		lines.push_back(line);
	}

	void saveDelaunayStruct(geometry::Triangulation struct_map, std::vector<geometry::Vector2D> pnts){
		if (pnts.size() == 0 || struct_map.size() == 0) {
			return;
		}
		std::vector<Point> state_points;
		std::vector<Line> state_lines;
		for (auto& pair : struct_map) {
			state_points.push_back(Point(pnts[pair.first.v1].x, pnts[pair.first.v1].y));
			state_points.push_back(Point(pnts[pair.first.v2].x, pnts[pair.first.v2].y));
			state_points.push_back(Point(pnts[pair.second.v1].x, pnts[pair.second.v1].y));

			state_lines.push_back(Line(Point(pnts[pair.first.v1].x, pnts[pair.first.v1].y), Point(pnts[pair.first.v2].x, pnts[pair.first.v2].y)));
			state_lines.push_back(Line(Point(pnts[pair.first.v1].x, pnts[pair.first.v1].y), Point(pnts[pair.second.v1].x, pnts[pair.second.v1].y)));
			state_lines.push_back(Line(Point(pnts[pair.first.v2].x, pnts[pair.first.v2].y), Point(pnts[pair.second.v1].x, pnts[pair.second.v1].y)));

		}
		points.push_back(state_points);
		lines.push_back(state_lines);
	}

	void add(std::vector<Point> linePoints){
		points.push_back(linePoints);
		std::vector<Line> lns;
		for (int i = 1;i < linePoints.size();i++) {
			Line l(linePoints[i], linePoints[i - 1]);
			lns.push_back(l);
		}
		lines.push_back(lns);
	}

	int stateCount() {
		return points.size();
	}

	void clear() {
		points.clear();
		lines.clear();
	}

	void drawLines() {
		if (lines.size() == 0) {
			return;
		}
		for (Line l : lines[0]) {
			l.draw(*window);
		}
	}

	bool animationRun() {
		return (stateCount() > 0);
	}

	void showState() {
		if (points.size() == 0) {
			previousTime = std::chrono::high_resolution_clock::now();
			return;
		}
		drawLines();
		drawPoints();

		std::chrono::steady_clock::time_point nextTime = std::chrono::high_resolution_clock::now();

		std::chrono::duration< double > fs = nextTime - previousTime;
		std::chrono::milliseconds d = std::chrono::duration_cast<std::chrono::milliseconds>(fs);
		if (d.count() > 700 && lines.size()!=1) {
			previousTime = nextTime;
			points.erase(points.begin());
			lines.erase(lines.begin());
		}
	}

private:
	std::chrono::steady_clock::time_point previousTime = std::chrono::high_resolution_clock::now();

	int stateNumber = -1;
	std::vector<std::vector<Point>> points;
	std::vector<std::vector<Line>> lines;
	sf::RenderWindow* window;
};