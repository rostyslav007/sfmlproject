#include <SFML/Graphics.hpp>
#include <string>


class Button {
public:

	Button(std::string txt, sf::Vector2f size, int charSize, sf::Color bgColor, sf::Color txtColor, sf::Font& font) {
		text.setFont(font);
		text.setString(txt);
		text.setFillColor(txtColor);
	
		text.setCharacterSize(charSize);

		button.setSize(size);
		button.setFillColor(bgColor);
	}

	void setBgColor(sf::Color color) {
		button.setFillColor(color);
	}

	void setTxtColor(sf::Color color) {
		text.setFillColor(color);
	}

	void setPosition(sf::Vector2f pos) {
		button.setPosition(pos);
		float xPos = (pos.x + button.getLocalBounds().width / 2) - (text.getLocalBounds().width / 2);
		float yPos = (pos.y + button.getLocalBounds().height / 2) - (text.getLocalBounds().height) + 5;

		text.setPosition(xPos, yPos);
	}

	void draw(sf::RenderWindow &window) {
		window.draw(button);
		window.draw(text);
	}

	float getWidth(){
		return button.getSize().x;
	}

	float getHeight() {
		return button.getSize().y;
	}

	bool isMouthOver(sf::RenderWindow& window) {
		int mouseX = sf::Mouse::getPosition(window).x;
		int mouseY = sf::Mouse::getPosition(window).y;

		float btnPosX = button.getPosition().x;
		float btnPosY = button.getPosition().y;

		float btnPosWidth = btnPosX + button.getLocalBounds().width;
		float btnPosHeight = btnPosY + button.getLocalBounds().height;

		return (mouseX > btnPosX && mouseX < btnPosWidth&& mouseY > btnPosY && mouseY < btnPosHeight);
	}
	

private:
	sf::RectangleShape button;
	sf::Text text;
};
