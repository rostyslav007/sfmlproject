#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>

class Point {
public:

	float x, y;
	Point():x(0),y(0),pointSize(0) {}

	Point(float x, float y):x(x), y(y){}

	Point(float x, float y, float size) : x(x), y(y), pointSize(size) {
		setPosition(x, y);
		circle.setRadius(size / 2);
	}

	void drawRed(sf::RenderWindow& window) {
		circle.setFillColor(sf::Color::Red);
		window.draw(circle);
	}

	void setPosition(float x, float y) {
		this->x = x;
		this->y = y;
		circle.setPosition({ x - pointSize / 2,y - pointSize / 2});
	}

	void setPointSize(float val) {
		pointSize = val;
	}

	void setColor(sf::Color color) {
		circle.setFillColor(color);
	}

	void draw(sf::RenderWindow& window) {
		window.draw(circle);
	}

	bool operator < (const Point& other) const {
		return this->x < other.x;
	}

	bool operator == (const Point& other) const {
		return (this->x == other.x) && (this->y == other.y);
	}

	bool operator > (const Point other) {
		return !(*this < other);
	}

	bool operator == (const Point other) {
		return (this->x == other.x && this->y == other.y);
	}

private:
	float pointSize = 0;
	sf::CircleShape circle;
};