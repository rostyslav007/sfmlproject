#pragma once
#include "Point.h"
#include <iostream>
#include <math.h>  
//#include <iostream>

class Line {
public:
	float k;
	float a, b, c;
	Point p1, p2;

	Line(Point p1, Point p2): p1(p1), p2(p2) {
		k = (p2.y - p1.y) / (p2.x - p1.x);
		a = p2.y - p1.y;
		b = p1.x - p2.x;
		c = -a * p1.x - b * p1.y;
	}

	bool operator < (const Line& other) const {
		return ((this->c + this->b + this->a + this->p1.x + this->p2.y) < (other.a + other.b + other.c + other.p1.x + other.p2.y));
	}
	
	bool operator==(const Line & other) const {
		return (this->p1 == other.p1) && (this->p2 == other.p2);
	}

	static float _innerProd(const Line l1, const Line l2) {
		float x1 = l1.p2.x - l1.p1.x, y1 = l1.p2.y - l1.p1.y,
			  x2 = l2.p2.x - l2.p1.x, y2 = l2.p2.y - l2.p1.y;
		return x1 * x2 + y1 * y2;
	}

	static float _vectorProd(const Line l1, const Line l2) {
		float x1 = l1.p2.x - l1.p1.x, y1 = l1.p2.y - l1.p1.y, 
			  x2 = l2.p2.x - l2.p1.x, y2 = l2.p2.y - l2.p1.y;
		return x1 * y2 - x2 * y1;
	}

	static bool _sameSide(const Line line, Point p1, Point p2) {
		float l_a = line.a, l_b = line.b, l_c = line.c;
		float norm_mult = 1 / sqrtf(l_a*l_a + l_b*l_b);
		float side1 = l_a * norm_mult * p1.x + l_b * norm_mult * p1.y + l_c * norm_mult;
		float side2 = l_a * norm_mult * p2.x + l_b * norm_mult * p2.y + l_c * norm_mult;

		return (side1 * side2 >= 0);
	}

	static Point _commonPoint(Line l1, Line l2) {
		if (l1.p1 == l2.p1 || l1.p1 == l2.p2) {
			return l1.p1;
		} 
		return l1.p2;
	}

	void draw(sf::RenderWindow& window) {

		sf::RectangleShape line;
		if (k >= 0) {
			line.setPosition({ std::min(p1.x, p2.x), std::min(p1.y, p2.y)-5 });
		}
		else {
			line.setPosition({ std::min(p1.x, p2.x), std::max(p1.y, p2.y)-5 });
		}
		line.setSize({ getLength(p1.x, p1.y, p2.x, p2.y), 10 });
		line.setFillColor(sf::Color::Green);
		line.setRotation(atan(k) * 180 / 3.1415);
		window.draw(line);
	}

	static bool inTriangleCircle(Line commonEdge, std::pair<Point, Point> p) {
		Point p1 = p.first;
		Point p2 = p.second;
		float sina = Line::sinBetweenLines(Line(commonEdge.p1, p1), Line(commonEdge.p2, p1)),
			cosa = Line::cosBetweenLines(Line(commonEdge.p1, p1), Line(commonEdge.p2, p1)),
			sinb = Line::sinBetweenLines(Line(commonEdge.p1, p2), Line(commonEdge.p2, p2)),
			cosb = Line::cosBetweenLines(Line(commonEdge.p1, p2), Line(commonEdge.p2, p2));
		float sin_a_b = sina * cosb + cosa * sinb;
		return (sin_a_b < 0);
	}

	static float cosBetweenLines(Line l1, Line l2) {
		float x1 = l1.p2.x - l1.p1.x, y1 = l1.p2.y - l1.p1.y,
			  x2 = l2.p2.x - l2.p1.x, y2 = l2.p2.y - l2.p1.y;
		return Line::_innerProd(l1, l2) / (sqrtf(x1 * x1 + y1 * y1) * sqrtf(x2 * x2 + y2 * y2));
	}

	static float sinBetweenLines(Line l1, Line l2) {
		float x1 = l1.p2.x - l1.p1.x, y1 = l1.p2.y - l1.p1.y,
			  x2 = l2.p2.x - l2.p1.x, y2 = l2.p2.y - l2.p1.y;
		return abs(Line::_vectorProd(l1, l2)) / (sqrtf(x1 * x1 + y1 * y1) * sqrtf(x2 * x2 + y2 * y2));
	}

	float getLength(float x1, float y1, float x2, float y2) {
		return sqrtf((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}
	
};